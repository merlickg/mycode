import requests

mtg_url = "https://api.magicthegathering.io/v1/cards"

def main():
    
    mtg_dic = {}
    i = 1

    while i < 3:
        mtg_cards = requests.get(f"{mtg_url}?page={i}")
        mtg_cards = mtg_cards.json()
        
        b = 0
        for crd in mtg_cards["cards"]:
            #print(crd["name"])
            #print(crd["rarity"])
            #mtg_dic.update({'name':'nnnn'})
            #mtg_dic[crd["name"]].append(crd["name"])
            #my_dict["Name"].append("Guru")
            
            #mtg_dic["name"] = crd["name"]
            #mtg_dic["rarity"] = crd["rarity"]
            if 'Rare' in crd.get("rarity"):
                b += 1
            
        i += 1

    print(f"Number of rare creatures on first 2 pages is: {b}")
    #print(len(mtg_cards["cards"]))

if __name__ == "__main__":
    main()
